﻿using System.IO;
using System.Collections.Generic;

namespace ClassLibrary
{
    public class Saver
    {
        private static Saver _saver;
        private Saver() { }
        public static Saver getSaver()
        {
            if (_saver == null)
                _saver = new Saver();
            return _saver;
        }
        public FileStream OpenCreateMyFile()
        {
            return new FileStream("D:\\КПЗ\\Lab4\\file.txt", FileMode.Open);
        }
        public void WriteMatrix(Matrix matrix)
        {
            FileStream fs = OpenCreateMyFile();
            StreamReader reader = new StreamReader(fs);
            int index=-1;
            List<string> lst = new List<string>(0);
            string s;
            while ((s = reader.ReadLine()) != "finish")
            {
                lst.Add(s);
                if(s=="")index=lst.Count-1;
            }
            reader.Close();
            fs.Close();
            string m = matrix.ToStringForFile();
            if (index == -1) lst.Add(m);
            else lst[index]=m;
            Clean();
            WriteFile(lst);
        }
        private void WriteFile(List<string> lst)
        {
            FileStream fs = OpenCreateMyFile();
            StreamWriter writer = new StreamWriter(fs);
            foreach (string ts in lst) writer.WriteLine(ts);
            writer.WriteLine("finish");
            writer.Close();
            fs.Close();
        }
        public Matrix ReadMatrix(int index)
        {
            List<string> lst = ListOfMatrix();
            if (lst[index] != "")
            {
                string str = lst[index].Substring(1, lst[index].Length - 2);
                string[] str2 = str.Split('|');
                for (int i = 0; i < str2.Length; i++)
                {
                    str2[i] = str2[i].Substring(1, str2[i].Length - 2);
                }
                List<string[]> str3 = new List<string[]>();
                for (int i = 0; i < str2.Length; i++)
                {
                    str3.Add(str2[i].Split(','));
                }
                double[,] matrix = new double[str3.Count, str3[0].Length];
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        matrix[i, j] = double.Parse(str3[i][j]);
                    }
                }
                return new Matrix(matrix);
            }
            return null;
        }
        public List<string> ListOfMatrix()
        {
            FileStream fs = OpenCreateMyFile();
            StreamReader reader = new StreamReader(fs);
            List<string> lst = new List<string>(0);
            string s;
            while ((s = reader.ReadLine()) != "finish")
            {
                lst.Add(s);
            }
            reader.Close();
            fs.Close();
            return lst;
        }
        public void ReplaceMatrix(Matrix matrix,int index)
        {
            string m = matrix.ToStringForFile();
            List<string> lst = ListOfMatrix();
            lst[index] = m;
            WriteFile(lst);
        }
        public void DeleteMatrix(int index)
        {
            List<string> lst= ListOfMatrix();
            lst[index] = "";
            WriteFile(lst);
        }
        public void Defragmentation()
        {
            List<string> lst=ListOfMatrix();
            while (lst.IndexOf("")!=-1)
            {
                lst.Remove("");
            }
            Clean();
            WriteFile(lst);
        }
        public void Clean()
        {
            System.IO.File.WriteAllText("D:\\КПЗ\\Lab4\\file.txt", string.Empty);
            FileStream fs = OpenCreateMyFile();
            StreamWriter writer = new StreamWriter(fs);
            writer.Write("finish");
            writer.Close();
            fs.Close();
        }
    }
}
