﻿using System;

namespace ClassLibrary
{
    public class Matrix
    {
        public double[,] Matr { get; private set; }
        public Matrix(int rows, int columns)
        {
            Matr = new double[rows, columns];
        }
        public Matrix(double[,] myMatrix)
        {
            Matr = new double[myMatrix.GetLength(0), myMatrix.GetLength(1)];
            for(int i = 0; i < Matr.GetLength(0); i++)
            {
                for (int j = 0; j < Matr.GetLength(1); j++)
                {
                    Matr[i,j]=myMatrix[i,j];
                }
            }
        }

        public void FillRandom()
        {
            Random random = new Random();
            for (int i = 0; i < Matr.GetLength(0); i++)
            {
                for (int j = 0; j < Matr.GetLength(1); j++)
                {
                    Matr[i, j] = random.Next(-20, 21);
                }
            }
        }
        public static Matrix operator *(Matrix a,Matrix b)
        {
            double[,] c = new double[a.Matr.GetLength(0), b.Matr.GetLength(1)];
            for (int i = 0; i < c.GetLength(0); i++)
            {
                for (int j = 0; j < c.GetLength(1); j++)
                {
                    for (int n = 0; n < a.Matr.GetLength(1); n++)
                    {
                        c[i, j] += a.Matr[i, n] * b.Matr[n, j];
                    }
                }
            }
            return new Matrix(c);
        }
        public double Determinant()
        {
            if(Matr.GetLength(0) == 2)
            {
                return Matr[0, 0] * Matr[1, 1] - Matr[0, 1] * Matr[1, 0];
            }
            else if(Matr.GetLength(0) == 1)
            {
                return Matr[0, 0];
            }
            else
            {
                Matrix copy = new Matrix(Matr);
                return HelpDet(copy);
            }
        }
        private double HelpDet(Matrix copy)
        {
            double tmp;
            for (int z = 0; z < copy.Matr.GetLength(0) - 1; z++)
            {
                for (int i = z; i < copy.Matr.GetLength(0) - 1; i++)
                {
                    int y = z + 1;
                    while (copy.Matr[z, z] == 0 && y < copy.Matr.GetLength(0))
                    {
                        for (int t = 0; t < copy.Matr.GetLength(0); t++)
                        {
                            tmp = copy.Matr[z, t];
                            copy.Matr[z, t] = copy.Matr[y, t];
                            copy.Matr[y, t] = tmp;
                        }
                        y++;
                    }
                    if (copy.Matr[z, z] == 0) break;
                    if (copy.Matr[i + 1, z] != 0)
                    {
                        double k = copy.Matr[i + 1, z] / copy.Matr[z, z];
                        for (int j = 0; j < copy.Matr.GetLength(0); j++)
                        {
                            copy.Matr[i+1, j] -=k * copy.Matr[z, j];
                        }
                    }
                }
                if (copy.Matr[z, z] == 0) break;
            }
            double res = 1;
            for(int i = 0; i < copy.Matr.GetLength(0); i++)
            {
                res*= copy.Matr[i, i];
            }
            return res;
        }
        private double AlgDop(int row,int col)
        {
            int r = 0;
            int c = 0;
            Matrix othermatr=new Matrix(Matr.GetLength(0)-1, Matr.GetLength(1)-1);
            for(int i = 0; i < Matr.GetLength(0); i++)
            {
                if (i != row)
                {
                    for (int j = 0; j < Matr.GetLength(1); j++)
                    {
                        if(j!= col)
                        {
                            othermatr.Matr[r, c] = Matr[i, j];
                            c++;
                            if (c == othermatr.Matr.GetLength(0))
                            {
                                c = 0;
                                r++;
                            }
                        }
                    }
                }
            }
            return HelpDet(othermatr) * Math.Pow(-1, row + col);
        }
        public Matrix Transponation()
        {
            Matrix tmatr=new Matrix(Matr.GetLength(1),Matr.GetLength(0));
            for(int i = 0; i < Matr.GetLength(0); i++)
            {
                for(int j = 0; j < Matr.GetLength(1); j++)
                {
                    tmatr.Matr[j, i] = Matr[i, j];
                }
            }
            return tmatr;
        }
        public Matrix MinusPow()
        {
            double det = Determinant();
            double[,] algmatr= new double[Matr.GetLength(0), Matr.GetLength(1)];
            if (Matr.GetLength(0) == 1)
            {
                return new Matrix(new double[1, 1] { { 1/Matr[0,0] } });
            }
            else if(Matr.GetLength(0) == 2)
            {
                algmatr[0,0]=Matr[1,1]/det;
                algmatr[0, 1] = Matr[1, 0] / det;
                algmatr[1, 0] = Matr[0, 1] / det;
                algmatr[1, 1] = Matr[0, 0] / det;
                return new Matrix(algmatr);
            }
            else
            {
                for (int i = 0; i < Matr.GetLength(0); i++)
                {
                    for(int j = 0; j < Matr.GetLength(1); j++)
                    {
                        algmatr[i,j] = AlgDop(i,j);
                    }
                }
                Matrix SouzTransMatr = new Matrix(new Matrix(algmatr).Transponation().Matr);
                for (int i = 0; i < Matr.GetLength(0); i++)
                {
                    for (int j = 0; j < Matr.GetLength(1); j++)
                    {
                        SouzTransMatr.Matr[i, j] /= det;
                    }
                }
                return SouzTransMatr;
            }
        }
        public string ToStringForFile()
        {
            string m = "{";
            for (int i = 0; i < Matr.GetLength(0); i++)
            {
                m += "{";
                for (int j = 0; j < Matr.GetLength(1); j++)
                {
                    m += Matr[i, j].ToString();
                    if (j + 1 != Matr.GetLength(1)) m += ",";
                }
                m += "}";
                if (i + 1 != Matr.GetLength(0)) m += "|";
            }
            m += "}";
            return m;
        }
    }
}
