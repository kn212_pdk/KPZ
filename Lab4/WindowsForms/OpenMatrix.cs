﻿using ClassLibrary;
using System;
using System.Windows.Forms;

namespace WindowsForms
{
    public partial class OpenMatrix : Form
    {
        public OpenMatrix()
        {
            InitializeComponent();
            IsClosed = true;
            saver = Saver.getSaver();
            numericUpDownIndex.Maximum = saver.ListOfMatrix().Count - 1;
            MyMatrix = saver.ReadMatrix(0);
            if(MyMatrix!=null)CreateGrid(MyMatrix);
        }
        Saver saver;
        public static bool IsClosed { get; private set; }
        public static Matrix MyMatrix { get; private set; }
        public static int Index { get; private set; }
        private void CreateGrid(Matrix m)
        {
            dataGridViewMatrix.RowCount = m.Matr.GetLength(0);
            dataGridViewMatrix.ColumnCount = m.Matr.GetLength(1);
            for (int i = 0; i < dataGridViewMatrix.RowCount; i++)
            {
                for (int j = 0; j < dataGridViewMatrix.ColumnCount; j++)
                {
                    dataGridViewMatrix[j, i].Value = m.Matr[i, j];
                    if (i == 0)
                    {
                        dataGridViewMatrix.Columns[j].HeaderText = j.ToString();
                    }
                }
                dataGridViewMatrix.Rows[i].HeaderCell.Value = i.ToString();
            }
        }
        private void buttonChoose_Click(object sender, EventArgs e)
        {
            Index = (int)numericUpDownIndex.Value;
            IsClosed = false;
            this.Close();
        }

        private void numericUpDownIndex_ValueChanged(object sender, EventArgs e)
        {
            MyMatrix = saver.ReadMatrix((int)numericUpDownIndex.Value);
            if (MyMatrix != null) CreateGrid(MyMatrix);
            else
            {
                dataGridViewMatrix.RowCount = 0;
                dataGridViewMatrix.ColumnCount = 0;
            }
        }
    }
}
