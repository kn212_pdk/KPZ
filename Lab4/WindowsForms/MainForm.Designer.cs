﻿namespace WindowsForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.createMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MultiplyMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FindDeterminantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransponationMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MinusPowMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenSaveMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReplaceMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DefragmentationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CleanFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewMatrix = new System.Windows.Forms.DataGridView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMatrix)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createMatrixToolStripMenuItem,
            this.MultiplyMatrixToolStripMenuItem,
            this.FindDeterminantToolStripMenuItem,
            this.TransponationMatrixToolStripMenuItem,
            this.MinusPowMatrixToolStripMenuItem,
            this.SaveMatrixToolStripMenuItem,
            this.OpenSaveMatrixToolStripMenuItem,
            this.ReplaceMatrixToolStripMenuItem,
            this.DeleteMatrixToolStripMenuItem,
            this.DefragmentationToolStripMenuItem,
            this.CleanFileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(261, 450);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // createMatrixToolStripMenuItem
            // 
            this.createMatrixToolStripMenuItem.Name = "createMatrixToolStripMenuItem";
            this.createMatrixToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.createMatrixToolStripMenuItem.Text = "Створити матрицю";
            this.createMatrixToolStripMenuItem.Click += new System.EventHandler(this.createMatrixToolStripMenuItem_Click);
            // 
            // MultiplyMatrixToolStripMenuItem
            // 
            this.MultiplyMatrixToolStripMenuItem.Name = "MultiplyMatrixToolStripMenuItem";
            this.MultiplyMatrixToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.MultiplyMatrixToolStripMenuItem.Text = "Помножити на іншу матрицю";
            this.MultiplyMatrixToolStripMenuItem.Visible = false;
            this.MultiplyMatrixToolStripMenuItem.Click += new System.EventHandler(this.MultiplyMatrixToolStripMenuItem_Click);
            // 
            // FindDeterminantToolStripMenuItem
            // 
            this.FindDeterminantToolStripMenuItem.Name = "FindDeterminantToolStripMenuItem";
            this.FindDeterminantToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.FindDeterminantToolStripMenuItem.Text = "Знайти визначник";
            this.FindDeterminantToolStripMenuItem.Visible = false;
            this.FindDeterminantToolStripMenuItem.Click += new System.EventHandler(this.FindDeterminantToolStripMenuItem_Click);
            // 
            // TransponationMatrixToolStripMenuItem
            // 
            this.TransponationMatrixToolStripMenuItem.Name = "TransponationMatrixToolStripMenuItem";
            this.TransponationMatrixToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.TransponationMatrixToolStripMenuItem.Text = "Знайти транспоновану матрицю";
            this.TransponationMatrixToolStripMenuItem.Visible = false;
            this.TransponationMatrixToolStripMenuItem.Click += new System.EventHandler(this.TransponationMatrixToolStripMenuItem_Click);
            // 
            // MinusPowMatrixToolStripMenuItem
            // 
            this.MinusPowMatrixToolStripMenuItem.Name = "MinusPowMatrixToolStripMenuItem";
            this.MinusPowMatrixToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.MinusPowMatrixToolStripMenuItem.Text = "Знайти обернену матрицю";
            this.MinusPowMatrixToolStripMenuItem.Visible = false;
            this.MinusPowMatrixToolStripMenuItem.Click += new System.EventHandler(this.MinusPowMatrixToolStripMenuItem_Click);
            // 
            // SaveMatrixToolStripMenuItem
            // 
            this.SaveMatrixToolStripMenuItem.Name = "SaveMatrixToolStripMenuItem";
            this.SaveMatrixToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.SaveMatrixToolStripMenuItem.Text = "Записати матрицю у файл";
            this.SaveMatrixToolStripMenuItem.Visible = false;
            this.SaveMatrixToolStripMenuItem.Click += new System.EventHandler(this.SaveMatrixToolStripMenuItem_Click);
            // 
            // OpenSaveMatrixToolStripMenuItem
            // 
            this.OpenSaveMatrixToolStripMenuItem.Name = "OpenSaveMatrixToolStripMenuItem";
            this.OpenSaveMatrixToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.OpenSaveMatrixToolStripMenuItem.Text = "Відкрити збережену матрицю";
            this.OpenSaveMatrixToolStripMenuItem.Click += new System.EventHandler(this.OpenSaveMatrixToolStripMenuItem_Click);
            // 
            // ReplaceMatrixToolStripMenuItem
            // 
            this.ReplaceMatrixToolStripMenuItem.Name = "ReplaceMatrixToolStripMenuItem";
            this.ReplaceMatrixToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.ReplaceMatrixToolStripMenuItem.Text = "Замінити збережену матрицю";
            this.ReplaceMatrixToolStripMenuItem.Visible = false;
            this.ReplaceMatrixToolStripMenuItem.Click += new System.EventHandler(this.ReplaceMatrixToolStripMenuItem_Click);
            // 
            // DeleteMatrixToolStripMenuItem
            // 
            this.DeleteMatrixToolStripMenuItem.Name = "DeleteMatrixToolStripMenuItem";
            this.DeleteMatrixToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.DeleteMatrixToolStripMenuItem.Text = "Видалити збережену матрицю";
            this.DeleteMatrixToolStripMenuItem.Click += new System.EventHandler(this.DeleteMatrixToolStripMenuItem_Click);
            // 
            // DefragmentationToolStripMenuItem
            // 
            this.DefragmentationToolStripMenuItem.Name = "DefragmentationToolStripMenuItem";
            this.DefragmentationToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.DefragmentationToolStripMenuItem.Text = "Виконати дефрагментацію файлу";
            this.DefragmentationToolStripMenuItem.Click += new System.EventHandler(this.DefragmentationToolStripMenuItem_Click);
            // 
            // CleanFileToolStripMenuItem
            // 
            this.CleanFileToolStripMenuItem.Name = "CleanFileToolStripMenuItem";
            this.CleanFileToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.CleanFileToolStripMenuItem.Text = "Очистити файл з матрицями";
            this.CleanFileToolStripMenuItem.Click += new System.EventHandler(this.CleanFileToolStripMenuItem_Click);
            // 
            // dataGridViewMatrix
            // 
            this.dataGridViewMatrix.AllowUserToAddRows = false;
            this.dataGridViewMatrix.AllowUserToDeleteRows = false;
            this.dataGridViewMatrix.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMatrix.Location = new System.Drawing.Point(283, 12);
            this.dataGridViewMatrix.Name = "dataGridViewMatrix";
            this.dataGridViewMatrix.RowHeadersWidth = 51;
            this.dataGridViewMatrix.RowTemplate.Height = 24;
            this.dataGridViewMatrix.Size = new System.Drawing.Size(531, 426);
            this.dataGridViewMatrix.TabIndex = 1;
            this.dataGridViewMatrix.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMatrix_CellValueChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 450);
            this.Controls.Add(this.dataGridViewMatrix);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMatrix)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem createMatrixToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridViewMatrix;
        private System.Windows.Forms.ToolStripMenuItem MultiplyMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FindDeterminantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TransponationMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MinusPowMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenSaveMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReplaceMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DefragmentationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CleanFileToolStripMenuItem;
    }
}

