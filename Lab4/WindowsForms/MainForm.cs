﻿using System;
using System.IO;
using System.Windows.Forms;
using ClassLibrary;

namespace WindowsForms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            FileInfo fileInfo = new FileInfo("D:\\КПЗ\\Lab4\\file.txt");
            if (fileInfo.Exists)
            {
                if (!File.ReadAllText(fileInfo.FullName).Contains("finish"))
                {
                    FileStream fileStream=new FileStream(fileInfo.FullName,FileMode.Open,FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fileStream);
                    sw.Write("finish");
                    sw.Close();
                    fileStream.Close();
                }
            }
            else fileInfo.Create();
            if (saver.ListOfMatrix().Count == 0)
            {
                ReplaceMatrixToolStripMenuItem.Visible=false;
                OpenSaveMatrixToolStripMenuItem.Visible=false;
                DeleteMatrixToolStripMenuItem.Visible = false;
                DefragmentationToolStripMenuItem.Visible = false;
                CleanFileToolStripMenuItem.Visible = false;
            }
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        }
        private Matrix matrix;
        Saver saver=Saver.getSaver();
        bool fl;
        private void CreateGrid(Matrix m)
        {
            fl = true;
            dataGridViewMatrix.RowCount = m.Matr.GetLength(0);
            dataGridViewMatrix.ColumnCount = m.Matr.GetLength(1);
            for (int i = 0; i < dataGridViewMatrix.RowCount; i++)
            {
                for (int j = 0; j < dataGridViewMatrix.ColumnCount; j++)
                {
                    dataGridViewMatrix[j, i].Value = m.Matr[i, j];
                    if (i == 0)
                    {
                        dataGridViewMatrix.Columns[j].HeaderText = j.ToString();
                    }
                }
                dataGridViewMatrix.Rows[i].HeaderCell.Value = i.ToString();
            }
            fl = false;
        }
        private void createMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateMatrix createMatrix = new CreateMatrix();
            createMatrix.ShowDialog();
            if (CreateMatrix.MyMatrix != null) {
                matrix = new Matrix(CreateMatrix.MyMatrix.Matr.GetLength(0), CreateMatrix.MyMatrix.Matr.GetLength(1));
                matrix.FillRandom();
                CreateGrid(matrix);
                ActiveItems();
            }
        }
        private void MultiplyMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            FillMatrix fillMatrix = new FillMatrix();
            fillMatrix.ShowDialog();
            if (FillMatrix.MyMatrix!=null && FillMatrix.MyMatrix.Matr.GetLength(0) == matrix.Matr.GetLength(1) && !FillMatrix.IsClosed) {
                Matrix newMatrix = matrix * FillMatrix.MyMatrix;
                matrix = new Matrix(newMatrix.Matr.GetLength(0), newMatrix.Matr.GetLength(1));
                dataGridViewMatrix.RowCount = newMatrix.Matr.GetLength(0);
                dataGridViewMatrix.ColumnCount = newMatrix.Matr.GetLength(1);
                for (int i = 0; i < dataGridViewMatrix.RowCount; i++)
                {
                    for (int j = 0; j < dataGridViewMatrix.ColumnCount; j++)
                    {
                        dataGridViewMatrix[j, i].Value = $"{newMatrix.Matr[i, j]:F2}";
                        matrix.Matr[i, j] = newMatrix.Matr[i, j];
                    }
                }
            }
            else
            {
                if(!FillMatrix.IsClosed && FillMatrix.MyMatrix.Matr.GetLength(0) != matrix.Matr.GetLength(1)) MessageBox.Show($"Кількість рядків другої матриці повинно дорівнювати кількості стовпчиків першої!", "Множення матриць", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void ActiveItems()
        {
            MultiplyMatrixToolStripMenuItem.Visible = dataGridViewMatrix.RowCount != 0 ? true : false;
            TransponationMatrixToolStripMenuItem.Visible = dataGridViewMatrix.RowCount != 0 ? true : false;
            MinusPowMatrixToolStripMenuItem.Visible = dataGridViewMatrix.RowCount == dataGridViewMatrix.ColumnCount && dataGridViewMatrix.RowCount != 0 ? true : false;
            FindDeterminantToolStripMenuItem.Visible = dataGridViewMatrix.RowCount == dataGridViewMatrix.ColumnCount && dataGridViewMatrix.RowCount != 0 ? true : false;
            SaveMatrixToolStripMenuItem.Visible = dataGridViewMatrix.RowCount != 0 ? true : false;
            ReplaceMatrixToolStripMenuItem.Visible = dataGridViewMatrix.RowCount != 0 ? true : false;
        }

        private void FindDeterminantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Детермінант даної матриці дорівнює {matrix.Determinant()}", "Детермінант", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void TransponationMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            matrix = new Matrix(matrix.Transponation().Matr);
            CreateGrid(matrix);
        }

        private void MinusPowMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (matrix.Determinant() == 0) MessageBox.Show("Не можна побудувати обернену матрицю, визначник дорівнює 0", "Обернена матриця", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                matrix = new Matrix(matrix.MinusPow().Matr);
                CreateGrid(matrix);
            }
        }

        private void SaveMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saver.WriteMatrix(matrix);
            OpenSaveMatrixToolStripMenuItem.Visible = true;
            DeleteMatrixToolStripMenuItem.Visible = true;
            DefragmentationToolStripMenuItem.Visible = true;
            CleanFileToolStripMenuItem.Visible = true;
            ReplaceMatrixToolStripMenuItem.Visible = true;
            MessageBox.Show("Матриця успішно збережена!", "Збереження матриці", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void OpenSaveMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenMatrix openMatrix = new OpenMatrix();
            openMatrix.ShowDialog();
            if (!OpenMatrix.IsClosed)
            {
                matrix = new Matrix(OpenMatrix.MyMatrix.Matr);
                CreateGrid(OpenMatrix.MyMatrix);
            }
            ActiveItems();
        }

        private void ReplaceMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenMatrix openMatrix = new OpenMatrix();
            openMatrix.ShowDialog();
            if (!OpenMatrix.IsClosed)
            {
                saver.ReplaceMatrix(matrix, OpenMatrix.Index);
                MessageBox.Show("Матриця успішно замінена!", "Заміна матриці", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void DeleteMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenMatrix openMatrix = new OpenMatrix();
            openMatrix.ShowDialog();
            if (!OpenMatrix.IsClosed)
            {
                saver.DeleteMatrix(OpenMatrix.Index);
                MessageBox.Show("Матриця успішно видалена!", "Видалення матриці", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void DefragmentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saver.Defragmentation();
            MessageBox.Show("Файл успішно дефрагментовано!", "Дефрагментація", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void CleanFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saver.Clean();
            MessageBox.Show("Файл успішно очищено!", "Очищення", MessageBoxButtons.OK, MessageBoxIcon.Information);
            OpenSaveMatrixToolStripMenuItem.Visible = false;
            DeleteMatrixToolStripMenuItem.Visible = false;
            DefragmentationToolStripMenuItem.Visible = false;
            CleanFileToolStripMenuItem.Visible = false;
            ReplaceMatrixToolStripMenuItem.Visible = false;
        }

        private void dataGridViewMatrix_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!fl)
            {
                matrix.Matr[e.RowIndex, e.ColumnIndex] = Convert.ToDouble(dataGridViewMatrix[e.ColumnIndex, e.RowIndex].Value);
            }
        }
    }
}