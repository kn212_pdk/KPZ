﻿using System;
using System.Windows.Forms;
using ClassLibrary;

namespace WindowsForms
{
    public partial class CreateMatrix : Form
    {
        public CreateMatrix()
        {
            InitializeComponent();
        }
        public static Matrix MyMatrix { get; private set; }
        private void buttonCreateMatrix_Click(object sender, EventArgs e)
        {
            MyMatrix = new Matrix((int)numericUpDownRow.Value,(int)numericUpDownColumn.Value);
            this.Close();
        }
    }
}
