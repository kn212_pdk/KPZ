﻿using System;
using System.Windows.Forms;
using ClassLibrary;

namespace WindowsForms
{
    public partial class FillMatrix : Form
    {
        public FillMatrix()
        {
            InitializeComponent();
            dataGridViewMatrix.RowCount = 1;
            dataGridViewMatrix.ColumnCount = 1;
            dataGridViewMatrix.Columns[0].HeaderText = "0";
            dataGridViewMatrix.Rows[0].HeaderCell.Value = "0";
            IsClosed = true;
        }
        public static Matrix MyMatrix { get; private set; }
        public static bool IsClosed { get; private set; }
        private void buttonOpenMatrix_Click(object sender, EventArgs e)
        {
            OpenMatrix openMatrix = new OpenMatrix();
            openMatrix.ShowDialog();
            if (!OpenMatrix.IsClosed)
            {
                MyMatrix = new Matrix(OpenMatrix.MyMatrix.Matr);
                numericUpDownRows.Value = MyMatrix.Matr.GetLength(0);
                dataGridViewMatrix.RowCount = MyMatrix.Matr.GetLength(0);
                numericUpDownColumns.Value = MyMatrix.Matr.GetLength(1);
                dataGridViewMatrix.ColumnCount = MyMatrix.Matr.GetLength(1);
                for (int i = 0; i < MyMatrix.Matr.GetLength(0); i++)
                {
                    for (int j = 0; j < MyMatrix.Matr.GetLength(1); j++)
                    {
                        if (i == 0)
                        {
                            dataGridViewMatrix.Columns[j].HeaderText = j.ToString();
                        }
                        dataGridViewMatrix[j, i].Value = MyMatrix.Matr[i, j];
                    }
                    dataGridViewMatrix.Rows[i].HeaderCell.Value = i.ToString();
                }
            }
        }

        private void buttonMultyplyMatrix_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridViewMatrix.RowCount; i++)
            {
                for (int j = 0; j < dataGridViewMatrix.ColumnCount; j++)
                {
                    MyMatrix.Matr[i, j] = Convert.ToInt64(dataGridViewMatrix[j, i].Value);
                }
            }
            IsClosed = false;
            this.Close();
        }

        private void numericUpDownRows_ValueChanged(object sender, EventArgs e)
        {
            dataGridViewMatrix.RowCount = (int)numericUpDownRows.Value;
            dataGridViewMatrix.Rows[(int)numericUpDownRows.Value-1].HeaderCell.Value = ((int)numericUpDownRows.Value - 1).ToString();
        }

        private void numericUpDownColumns_ValueChanged(object sender, EventArgs e)
        {
            dataGridViewMatrix.ColumnCount = (int)numericUpDownColumns.Value;
            dataGridViewMatrix.Columns[(int)numericUpDownColumns.Value-1].HeaderText = ((int)numericUpDownColumns.Value - 1).ToString();
        }
    }
}
