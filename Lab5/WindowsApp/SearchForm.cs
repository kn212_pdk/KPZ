﻿using ClassLibrary;
using System;
using System.Windows.Forms;

namespace WindowsApp
{
    public partial class SearchForm : Form
    {
        public SearchForm()
        {
            InitializeComponent();
        }
        public bool FlClose { get;private set; }
        public string WordName { get;private set; }
        public int IndexLang { private get; set; }
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            WordName = comboBoxWord.SelectedItem.ToString();
            FlClose = true;
            this.Close();
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            FasadeFileBase fasadeFile = new FasadeFileBase(IndexLang);
            string[] s = fasadeFile.ListOfNames();
            Array.Sort(s);
            for (int i = 0; i < s.Length; i++)
            {
                comboBoxWord.Items.Add(s[i]);
            }
            comboBoxWord.SelectedIndex = 0;
            FlClose = false;
        }
    }
}
