﻿namespace WindowsApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelOriginal = new System.Windows.Forms.Label();
            this.labelTranslate = new System.Windows.Forms.Label();
            this.comboBoxOriginal = new System.Windows.Forms.ComboBox();
            this.panelOriginal = new System.Windows.Forms.Panel();
            this.textBoxOriginal = new System.Windows.Forms.TextBox();
            this.panelTranslate = new System.Windows.Forms.Panel();
            this.textBoxTranslate = new System.Windows.Forms.TextBox();
            this.buttonAddWord = new System.Windows.Forms.Button();
            this.buttonRemoveWord = new System.Windows.Forms.Button();
            this.buttonTranslate = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.panelOriginal.SuspendLayout();
            this.panelTranslate.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelOriginal
            // 
            this.labelOriginal.AutoSize = true;
            this.labelOriginal.Font = new System.Drawing.Font("Comic Sans MS", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelOriginal.Location = new System.Drawing.Point(7, 10);
            this.labelOriginal.Name = "labelOriginal";
            this.labelOriginal.Size = new System.Drawing.Size(205, 32);
            this.labelOriginal.TabIndex = 0;
            this.labelOriginal.Text = "Мова оригіналу:";
            // 
            // labelTranslate
            // 
            this.labelTranslate.AutoSize = true;
            this.labelTranslate.Font = new System.Drawing.Font("Comic Sans MS", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTranslate.Location = new System.Drawing.Point(7, 8);
            this.labelTranslate.Name = "labelTranslate";
            this.labelTranslate.Size = new System.Drawing.Size(342, 32);
            this.labelTranslate.TabIndex = 1;
            this.labelTranslate.Text = "Мова перекладу: англійська";
            // 
            // comboBoxOriginal
            // 
            this.comboBoxOriginal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxOriginal.FormattingEnabled = true;
            this.comboBoxOriginal.Items.AddRange(new object[] {
            "українська",
            "англійська"});
            this.comboBoxOriginal.Location = new System.Drawing.Point(231, 13);
            this.comboBoxOriginal.Name = "comboBoxOriginal";
            this.comboBoxOriginal.Size = new System.Drawing.Size(182, 33);
            this.comboBoxOriginal.TabIndex = 2;
            this.comboBoxOriginal.SelectedIndexChanged += new System.EventHandler(this.comboBoxOriginal_SelectedIndexChanged);
            // 
            // panelOriginal
            // 
            this.panelOriginal.BackColor = System.Drawing.Color.IndianRed;
            this.panelOriginal.Controls.Add(this.buttonSearch);
            this.panelOriginal.Controls.Add(this.textBoxOriginal);
            this.panelOriginal.Controls.Add(this.comboBoxOriginal);
            this.panelOriginal.Controls.Add(this.labelOriginal);
            this.panelOriginal.Location = new System.Drawing.Point(12, 12);
            this.panelOriginal.Name = "panelOriginal";
            this.panelOriginal.Size = new System.Drawing.Size(424, 159);
            this.panelOriginal.TabIndex = 4;
            // 
            // textBoxOriginal
            // 
            this.textBoxOriginal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxOriginal.Location = new System.Drawing.Point(13, 60);
            this.textBoxOriginal.Name = "textBoxOriginal";
            this.textBoxOriginal.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBoxOriginal.Size = new System.Drawing.Size(400, 34);
            this.textBoxOriginal.TabIndex = 3;
            // 
            // panelTranslate
            // 
            this.panelTranslate.BackColor = System.Drawing.Color.RoyalBlue;
            this.panelTranslate.Controls.Add(this.textBoxTranslate);
            this.panelTranslate.Controls.Add(this.labelTranslate);
            this.panelTranslate.Location = new System.Drawing.Point(12, 177);
            this.panelTranslate.Name = "panelTranslate";
            this.panelTranslate.Size = new System.Drawing.Size(424, 202);
            this.panelTranslate.TabIndex = 5;
            // 
            // textBoxTranslate
            // 
            this.textBoxTranslate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxTranslate.Location = new System.Drawing.Point(13, 55);
            this.textBoxTranslate.Multiline = true;
            this.textBoxTranslate.Name = "textBoxTranslate";
            this.textBoxTranslate.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBoxTranslate.Size = new System.Drawing.Size(398, 105);
            this.textBoxTranslate.TabIndex = 4;
            // 
            // buttonAddWord
            // 
            this.buttonAddWord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonAddWord.Font = new System.Drawing.Font("Comic Sans MS", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddWord.Location = new System.Drawing.Point(12, 452);
            this.buttonAddWord.Name = "buttonAddWord";
            this.buttonAddWord.Size = new System.Drawing.Size(424, 61);
            this.buttonAddWord.TabIndex = 6;
            this.buttonAddWord.Text = "Додати слово в словник";
            this.buttonAddWord.UseVisualStyleBackColor = false;
            this.buttonAddWord.Click += new System.EventHandler(this.buttonAddWord_Click);
            // 
            // buttonRemoveWord
            // 
            this.buttonRemoveWord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonRemoveWord.Font = new System.Drawing.Font("Comic Sans MS", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRemoveWord.Location = new System.Drawing.Point(12, 519);
            this.buttonRemoveWord.Name = "buttonRemoveWord";
            this.buttonRemoveWord.Size = new System.Drawing.Size(424, 61);
            this.buttonRemoveWord.TabIndex = 7;
            this.buttonRemoveWord.Text = "Видалити слово зі словника";
            this.buttonRemoveWord.UseVisualStyleBackColor = false;
            this.buttonRemoveWord.Click += new System.EventHandler(this.buttonRemoveWord_Click);
            // 
            // buttonTranslate
            // 
            this.buttonTranslate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonTranslate.Font = new System.Drawing.Font("Comic Sans MS", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonTranslate.Location = new System.Drawing.Point(12, 385);
            this.buttonTranslate.Name = "buttonTranslate";
            this.buttonTranslate.Size = new System.Drawing.Size(424, 61);
            this.buttonTranslate.TabIndex = 8;
            this.buttonTranslate.Text = "Перекласти";
            this.buttonTranslate.UseVisualStyleBackColor = false;
            this.buttonTranslate.Click += new System.EventHandler(this.buttonTranslate_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonSearch.Font = new System.Drawing.Font("Comic Sans MS", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSearch.Location = new System.Drawing.Point(13, 100);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(400, 48);
            this.buttonSearch.TabIndex = 9;
            this.buttonSearch.Text = "Вибрати слово із словника";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 596);
            this.Controls.Add(this.buttonTranslate);
            this.Controls.Add(this.buttonRemoveWord);
            this.Controls.Add(this.buttonAddWord);
            this.Controls.Add(this.panelTranslate);
            this.Controls.Add(this.panelOriginal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "Словник";
            this.panelOriginal.ResumeLayout(false);
            this.panelOriginal.PerformLayout();
            this.panelTranslate.ResumeLayout(false);
            this.panelTranslate.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelOriginal;
        private System.Windows.Forms.Label labelTranslate;
        private System.Windows.Forms.ComboBox comboBoxOriginal;
        private System.Windows.Forms.Panel panelOriginal;
        private System.Windows.Forms.TextBox textBoxOriginal;
        private System.Windows.Forms.Panel panelTranslate;
        private System.Windows.Forms.TextBox textBoxTranslate;
        private System.Windows.Forms.Button buttonAddWord;
        private System.Windows.Forms.Button buttonRemoveWord;
        private System.Windows.Forms.Button buttonTranslate;
        private System.Windows.Forms.Button buttonSearch;
    }
}

