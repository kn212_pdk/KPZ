﻿using ClassLibrary;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace WindowsApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            comboBoxOriginal.SelectedIndex = 0;
        }
        FasadeFileBase fasadeFile;
        private void comboBoxOriginal_SelectedIndexChanged(object sender, EventArgs e)
        {
            fasadeFile = new FasadeFileBase(comboBoxOriginal.SelectedIndex);
            if(comboBoxOriginal.SelectedIndex==0)labelTranslate.Text = "Мова перекладу: англійська";
            else labelTranslate.Text = "Мова перекладу: українська";
        }

        private void buttonTranslate_Click(object sender, EventArgs e)
        {
            textBoxTranslate.Text = "";
            if (LanguageRight())
            {
                string translation=fasadeFile.Translation(textBoxOriginal.Text);
                if (translation != null) textBoxTranslate.Text = translation;
                else textBoxTranslate.Text = "Переклад не знайдено!";
            }
        }

        private void buttonAddWord_Click(object sender, EventArgs e)
        {
            if (LanguageRight() && textBoxTranslate.Text != "")
            {
                if (fasadeFile.AddNewWord(textBoxOriginal.Text, textBoxTranslate.Text)) MessageBox.Show("Слово успішно додано", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    if(MessageBox.Show("Це слово вже є в словнику.Замінити?", "Повідомлення", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        fasadeFile.RemoveWord(textBoxOriginal.Text);
                        fasadeFile.AddNewWord(textBoxOriginal.Text, textBoxTranslate.Text);
                        MessageBox.Show("Слово успішно замінено", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void buttonRemoveWord_Click(object sender, EventArgs e)//треба перевірка слова на наявність
        {
            textBoxTranslate.Text = "";
            if (LanguageRight())
            {
                if (fasadeFile.RemoveWord(textBoxOriginal.Text)) MessageBox.Show("Слово успішно видалено", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else MessageBox.Show("В словнику немає такого слова!", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private bool LanguageRight()
        {
            Regex regex = new Regex("[a-z]+",RegexOptions.IgnoreCase);
            if (comboBoxOriginal.SelectedIndex == 0)
            {
                if (!regex.IsMatch(textBoxOriginal.Text) && (regex.IsMatch(textBoxTranslate.Text) || textBoxTranslate.Text==""))return true;
                else
                {
                    MessageBox.Show("Слово написано не кирилецею чи/та переклад не латиницею", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                if (regex.IsMatch(textBoxOriginal.Text) && (!regex.IsMatch(textBoxTranslate.Text) || textBoxTranslate.Text == "")) return true;
                else
                {
                    MessageBox.Show("Слово написано не латиницею чи/та переклад не кирилецею", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            SearchForm searchForm = new SearchForm(); 
            searchForm.IndexLang = comboBoxOriginal.SelectedIndex;
            searchForm.ShowDialog();
            if (searchForm.FlClose) textBoxOriginal.Text = searchForm.WordName;
        }
    }
}
