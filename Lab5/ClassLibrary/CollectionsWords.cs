﻿using System.Collections.Generic;

namespace ClassLibrary
{
    internal class CollectionsWords
    {
        private List<Word> Words;
        public Word this[int index]
        {
            get => Words[index];
            set => Words[index] = value;
        }
        public int Count()
        {
            return Words.Count;
        }
        public CollectionsWords()
        {
            Words = new List<Word>();
        }
        public bool AddWord(Word word)
        {
            bool fl = true;
            for (int i = 0; i < Words.Count; i++)
            {
                if(Words[i].Name == word.Name)fl= false;
            }
            if (fl)
            {
                Words.Add(word);
                return true;
            }
            return false;
        }
        public bool DeleteWord(string name)
        {
            for (int i = 0; i < Words.Count; i++)
            {
                if (Words[i].Name == name)
                {
                    Words.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }
    }
}
