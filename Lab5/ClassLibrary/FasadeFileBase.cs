﻿namespace ClassLibrary
{
    public class FasadeFileBase
    {
        private FileWork FileSystem;
        private CollectionsWords Words;
        private string Lang;
        public FasadeFileBase(int ilang)
        {
            if (ilang == 0) Lang = "uk-en";
            else Lang = "en-uk";
            FileSystem = new FileWork();
            Words = FileSystem.Load($"D:\\КПЗ\\Lab5\\{Lang}.txt");
        }
        public bool AddNewWord(string name, string value)
        {
            if (Words.AddWord(new Word(name, value)))
            {
                FileSystem.Write($"D:\\КПЗ\\Lab5\\{Lang}.txt", Words);
                return true;
            }
            return false;
        }
        public bool RemoveWord(string name)
        {
            if (Words.DeleteWord(name))
            {
                FileSystem.Write($"D:\\КПЗ\\Lab5\\{Lang}.txt", Words);
                return true;
            }
            return false;
        }
        public string Translation(string name)
        {
            if (Words != null)
                for (int i = 0; i < Words.Count(); i++)
                {
                    if (name == Words[i].Name) return Words[i].Value;
                }
            return null;
        }
        public string[] ListOfNames()
        {
            string[] names = new string[Words.Count()];
            for(int i = 0; i < names.Length; i++)
            {
                names[i]=Words[i].Name;
            }
            return names;
        }
    }
}
