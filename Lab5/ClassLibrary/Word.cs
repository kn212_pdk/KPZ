﻿namespace ClassLibrary
{
    internal class Word
    {
        public string Name { get; private set; }
        public string Value { get; private set; }
        public Word(string name,string value)
        {
           Name = name;
           Value = value;
        }
    }
}
