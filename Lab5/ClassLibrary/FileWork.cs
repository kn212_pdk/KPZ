﻿using System.Collections.Generic;
using System.IO;

namespace ClassLibrary
{
    internal class FileWork
    {
        public CollectionsWords Load(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);
            StreamReader reader = new StreamReader(fs);
            List<string> lst= new List<string>();
            CollectionsWords words = new CollectionsWords();
            string s= reader.ReadLine();
            while (s != "" && s != null)
            {
                lst.Add(s);
                s = reader.ReadLine();
            }
            reader.Close();
            fs.Close();
            if (lst.Count > 0)
            {
                for (int i = 0; i < lst.Count; i++)
                {
                    string[] arr = lst[i].Split('=');
                    words.AddWord(new Word(arr[0], arr[1]));
                }
            }
            return words;
        }
        public void Write(string filename,CollectionsWords words)
        {
            System.IO.File.WriteAllText(filename, string.Empty);
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);
            StreamWriter writer = new StreamWriter(fs);
            for(int i = 0; i < words.Count(); i++)
            {
                string s= words[i].Name + "=" + words[i].Value;
                writer.WriteLine(s);
            }
            writer.Close();
            fs.Close();
        }
    }
}
