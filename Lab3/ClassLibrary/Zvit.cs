﻿namespace ClassLibrary
{
    public class Zvit
    {
        private Zvit() { }
        private static Zvit _zvit;
        public static Zvit Zvitnist()
        {
            if (_zvit == null)
            {
                _zvit = new Zvit();
            }
            return _zvit;
        }
        public void PrybNakladna(Sklad sklad,Product [] products)
        {
            Console.WriteLine("Прибуткова накладна");
            DateTime date = DateTime.Now;
            for (int i = 0; i < products.Length; i++)
            {
                Console.WriteLine($"--------Продукт №{i}-------");
                products[i].PribytokInfo(products[i].NumInPartia, date);
                sklad.Products.Add(products[i]);
            }
            Console.WriteLine($"Прийнято:{date}");
        }
        public void VydatNakladna(Sklad sklad,int[] number,int[] numOfprod)
        {
            double sum = 0;
            Console.WriteLine("Вибуткова накладна");

            for (int i = 0; i < number.Length; i++)
            {
                Console.WriteLine($"-------Продукт №{i}-------");
                sklad.Products[number[i]].VybotokInfo(numOfprod[i]);
            }
            sklad.RemoveProduct(number, numOfprod);
            Console.WriteLine($"Відпущено:{DateTime.Now}");
        }
        public void Inventarisation(Sklad sklad)
        {
            Console.WriteLine("Iнвертизація");
            sklad.ShowSklad();
            Console.WriteLine($"Перевірено:{DateTime.Now}");
        }
    }
}
