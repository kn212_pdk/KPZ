﻿namespace ClassLibrary
{
    public class Product:Money
    {
        public string Name { protected set; get; }
        public string NameOfUnit { protected set; get; }
        public DateTime DateOfDelivery { set; get; }
        public int NumInPartia { get; set; }
        public Product(string name,string nameOfUnit,int bigmoney, int smallmoney, string valute, int numInPartia) : base(bigmoney, smallmoney, valute)
        {
            Name = name;
            NameOfUnit = nameOfUnit;
            NumInPartia = numInPartia;
        }
        public bool RemovePrice(double action)
        {
            if ((int)(action * 100) <= (BigMoney * 100 + SmallMoney))
            {
                BigMoney -= (int)action;
                SmallMoney -= (int)(action * 100) % 100;
                while (SmallMoney < 0)
                {
                    BigMoney--;
                    SmallMoney += 100;
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public void PribytokInfo(int number,DateTime data)
        {
            Console.WriteLine($"Назва:{Name}");
            Console.WriteLine($"Кількість:{number} {NameOfUnit}");
            Console.WriteLine($"Ціна одиниці:{MoneyString()}");
            DateOfDelivery = data;
        }
        public void VybotokInfo(int number)
        {
            Console.WriteLine($"Назва:{Name}");
            Console.WriteLine($"Кількість:{number} {NameOfUnit}");
            Console.WriteLine($"Ціна одиниці:{MoneyString()}");
        }
    }
}
