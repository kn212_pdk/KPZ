﻿namespace ClassLibrary
{
    public class Money
    {
        public int BigMoney { protected set; get; }
        public int SmallMoney { protected set; get; }
        public string Valute { protected set; get; }
        public Money(int bigMoney, int smallMoney, string valute)
        {
            BigMoney = bigMoney;
            SmallMoney = smallMoney;
            Valute = valute;
        }
        public string MoneyString()
        {
            return $"{BigMoney+SmallMoney/100.0} {Valute}";
        }
    }
}