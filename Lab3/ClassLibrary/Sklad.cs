﻿namespace ClassLibrary
{
    public class Sklad
    {
        public List<Product> Products { get;set; }
        private Sklad() 
        {
            Products = new List<Product>();
        }
        private static Sklad _sklad;
        public static Sklad OpenSklad()
        {
            if( _sklad == null)
            {
                _sklad = new Sklad();
            }
            return _sklad;
        }
        internal void RemoveProduct(int[] numbers, int[]numberofprod)
        {
            int tmp,tmp2;
            for(int i=0; i<numbers.Length; i++)
            {
                for(int j=i+1; j<numbers.Length; j++)
                {
                    if (numbers[j] < numbers[i])
                    {
                        tmp=numbers[j];
                        numbers[j]=numbers[i];
                        numbers[i]=tmp;
                        tmp2 = numberofprod[j];
                        numberofprod[j] = numberofprod[i];
                        numberofprod[i] = tmp2;
                    }
                }
            }
            for (int i = numbers.Length-1; i >= 0 ; i--)
            {
                if (numberofprod[i] == Products[numbers[i]].NumInPartia)
                {
                    Products.RemoveAt(numbers[i]);
                }
                else Products[numbers[i]].NumInPartia -= numberofprod[i];
            }
        }
        public void ShowSklad()
        {
            for (int i = 0; i < Products.Count; i++)
            {
                Console.WriteLine($"-----------Продукт№{i}----------------");
                Console.WriteLine($"Назва:{Products[i].Name}");
                Console.WriteLine($"Кількість:{Products[i].NumInPartia} {Products[i].NameOfUnit}");
                Console.WriteLine($"Ціна одиниці:{Products[i].MoneyString()}");
                Console.WriteLine($"Дата останього завозу:{Products[i].DateOfDelivery}");
            }
        }
    }
}
