﻿using ClassLibrary;
namespace Test
{
    class Program
    {
        private static void Main()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.InputEncoding = System.Text.Encoding.Unicode;

            int y = 0;
            Zvit zvitnist = Zvit.Zvitnist();
            Sklad sklad = Sklad.OpenSklad();
            do
            {
                Console.WriteLine("0-Додати продукт(и)\n1-Оглянути склад\n2-Продати продукт(и)\nінше-вийти");
                y = WriteNum(y);
                switch (y)
                {
                    case 0:Console.WriteLine("Введіть кількість замовлень:");
                        int n=0;
                        n=WriteNum(n);
                        Product[] products = new Product[n];
                        for(int i = 0; i < products.Length; i++)
                        {
                            Console.WriteLine("Введіть назву продукту");
                            string name=Console.ReadLine();
                            Console.WriteLine("Введіть назву одиниці");
                            string unit=Console.ReadLine();
                            Console.WriteLine("Введіть кількість одиниць");
                            int num=0;
                            num=WriteNum(num);
                            Console.WriteLine("Введіть ціну одиниці(приклад 10.00 грн/20,25 грн )");
                            string[] money = Console.ReadLine().Split(' ', '.', ',');
                            while(money.Length!=3|| !int.TryParse(money[0],out int result) || !int.TryParse(money[1], out int result2))
                            {
                                money = Console.ReadLine().Split(' ', '.', ',');
                            }
                            int bigM = int.Parse(money[0]);
                            int smallM = int.Parse(money[1]);
                            string valute=money[2];
                            products[i] = new Product(name, unit, bigM, smallM, valute, num);
                        }
                        zvitnist.PrybNakladna(sklad, products);
                        break;
                    case 1: if (sklad.Products.Count == 0) Console.WriteLine("Склад пустий!");
                        else zvitnist.Inventarisation(sklad);
                        break;
                    case 2:
                        if (sklad.Products.Count == 0) Console.WriteLine("Склад пустий!");
                        else
                        {
                            Console.WriteLine("Введіть кількість замовлень:");
                            n = 0;
                            n=WriteNum(n);
                            int[] indexs = new int[n];
                            int[] numbers = new int[n];
                            for (int i = 0; i < indexs.Length; i++)
                            {
                                Console.WriteLine("Введіть індекс продукту на складі");
                                indexs[i]=WriteNum(indexs[i]);
                                while (numbers[i] > sklad.Products[indexs[i]].NumInPartia)
                                {
                                    Console.WriteLine($"Продукту з таким індексом немає!Введіть індекс від 0 до {sklad.Products[sklad.Products.Count-1]}");
                                    numbers[i]=WriteNum(numbers[i]);
                                }
                                Console.WriteLine("Введіть кількість одиниць продукту");
                                numbers[i]=WriteNum(numbers[i]);
                                while (numbers[i] > sklad.Products[indexs[i]].NumInPartia)
                                {
                                    Console.WriteLine($"Багато!На складі лише {sklad.Products[indexs[i]].NumInPartia} одиниць.\nВведіть кількість одиниць продукту");
                                    numbers[i]=WriteNum(numbers[i]);
                                }
                            }
                            zvitnist.VydatNakladna(sklad, indexs, numbers);
                        }
                        break;
                    default: break;
                }
            } while (y < 3);
        }
        private static int WriteNum(int value)
        {
            while (!int.TryParse(Console.ReadLine(), out value))
            {
                Console.Write("Введіть число:");
            }
            return value;
        }
    }
}
