﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class Trombone : Instrument
    {
        protected override string Name { get; set; }
        protected override string Description { get; set; }
        protected override string History { get; set; }
        public Trombone()
        {
            Name = "Тромбон";
            Description = "Звичайний тромбон";
            History = "Тромбон — музичний інструмент сімейства мідних духових.";
        }
        public Trombone(string name, string description, string history)
        {
            Name = name;
            Description = description;
            History = history;
        }
        public Trombone(string name, string history)
        {
            Name = name;
            Description = "Звичайна укулеле з нейлоновими струнами";
            History = history;
        }
        public Trombone(Trombone trombone)
        {
            Name = trombone.Name;
            Description = trombone.Description;
            History = trombone.History;
        }
        public override void Desc()
        {
            Console.WriteLine(Description);
        }
        public override void HistoryTool()
        {
            Console.WriteLine(History);
        }
        public override void Show()
        {
            Console.WriteLine(Name);
        }
        public override void Sound()
        {
            Console.WriteLine("Звук інструменту: Ду-дуу-дууу-ду-ду");
        }
        public override string ToString()
        {
            return $"({Name}, {History}) - {Description}";
        }
    }
}
