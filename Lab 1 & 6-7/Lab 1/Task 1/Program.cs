﻿using Task_1;
using Task_1.InstrumentTool;
using System;
using System.Text;

Instrument[] tools = new Instrument[4] { new Violin(), new Trombone(), new Ukulele(), new Cello() };

    Console.OutputEncoding = Encoding.Unicode;
    Console.InputEncoding = Encoding.Unicode;
for (int i = 0; i < tools.Length; i++)
{
    tools[i].Sound();
    tools[i].Show();
    tools[i].Desc();
    tools[i].HistoryTool();
    Console.WriteLine(tools[i].ToString());
    Console.WriteLine();
}