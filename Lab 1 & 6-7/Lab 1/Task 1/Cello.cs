﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class Cello : Instrument
    {
        protected override string Name { get; set; }
        protected override string Description { get; set; }
        protected override string History { get; set; }
        public Cello()
        {
            Name = "Віолончель";
            Description = "Звичайна віолончель";
            History = "Віолончель — струнний смичковий музичний інструмент, родини скрипкових, басо-тенорового регістру. ";
        }
        public Cello(string name, string description, string history)
        {
            Name = name;
            Description = description;
            History = history;
        }
        public Cello(string name, string history)
        {
            Name = name;
            Description = "Звичайна віолончель";
            History = history;
        }
        public Cello(Cello cello)
        {
            Name = cello.Name;
            Description = cello.Description;
            History = cello.History;
        }
        public override void Desc()
        {
            Console.WriteLine(Description);
        }
        public override void HistoryTool()
        {
            Console.WriteLine(History);
        }
        public override void Show()
        {
            Console.WriteLine(Name);
        }
        public override void Sound()
        {
            Console.WriteLine("Звук інструменту: Ту-туу-туу-ту-ру-ту");
        }
        public override string ToString()
        {
            return $"({Name}, {History}) - {Description}";
        }
    }
}
