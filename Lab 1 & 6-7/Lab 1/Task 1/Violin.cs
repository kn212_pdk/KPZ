﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1.InstrumentTool
{
    public class Violin : Instrument
    {
        protected override string Name { get; set; }
        protected override string Description { get; set; }
        protected override string History { get; set; }

        public Violin()
        {
            Name = "Скрипка";
            Description = "Звичайна скрипка зі смичком";
            History = "Прототипами скрипки були арабський ребаб і німецька рота," +
                " злиття яких і утворило віолу. Форми скрипки сформувались до XVI століття";
        }
        public Violin(string name, string description, string history)
        {
            Name = name;
            Description = description;
            History = history;
        }
        public Violin(string name, string history)
        {
            Name = name;
            Description = "Пустий шаблон";
            History = history;
        }
        public Violin(string name)
        {
            Name = name;
            Description = "Пустий шаблон";
            History = "Звичайна скрипка зі смичком";
        }
        public Violin(Violin violin)
        {
            Name = violin.Name;
            Description = violin.Description;
            History = violin.History;
        }
        public override void Desc()
        {
            Console.WriteLine(Description);
        }
        public override void HistoryTool()
        {
            Console.WriteLine(History);
        }
        public override void Show()
        {
            Console.WriteLine(Name);
        }
        public override void Sound()
        {
            Console.WriteLine("Звук інструменту: Ві-віі-віііі");
        }
        public override string ToString()
        {
            return $"({Name}, {History}) - {Description}";
        }
    }
}
