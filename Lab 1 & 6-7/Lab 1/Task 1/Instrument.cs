﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public abstract class Instrument
    {
        protected abstract string Name { get; set; }
        protected abstract string Description { get; set; }
        protected abstract string History { get; set; }

        public void SetName(string name) => Name = name;
        public string GetName() => Name;
        public void SetDescription(string decs) => Description = decs;
        public string GetDescriprion() => Description;
        public void SetHistory(string history) => History = history;
        public string GetHistory() => History;
        public abstract void Sound();
        public abstract void Show();
        public abstract void Desc();
        public abstract void HistoryTool();
    }
}
