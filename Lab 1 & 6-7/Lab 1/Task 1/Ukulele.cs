﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class Ukulele : Instrument
    {
        protected override string Name { get; set; }
        protected override string Description { get; set; }
        protected override string History { get; set; }
        public Ukulele()
        {
            Name = "Укулеле";
            Description = "Звичайна укулеле з нейлоновими струнами";
            History = "Укулеле — невеликий інструмент, що схожий на гітару, який був завезений на Гаваї португальськими" +
                " іммігрантами з Мадейри. На початку 20 століття він набув великої популярності в решті місць Сполучених Штатів," +
                " звідки поширився і здобув популярність на міжнародному рівні.";
        }
        public Ukulele(string name, string description, string history)
        {
            Name = name;
            Description = description;
            History = history;
        }
        public Ukulele(string name, string history)
        {
            Name = name;
            Description = "Звичайна укулеле з нейлоновими струнами";
            History = history;
        }
        public Ukulele(Ukulele ukulele)
        {
            Name = ukulele.Name;
            Description = ukulele.Description;
            History = ukulele.History;
        }
        public override void Desc()
        {
            Console.WriteLine(Description);
        }
        public override void HistoryTool()
        {
            Console.WriteLine(History);
        }
        public override void Show()
        {
            Console.WriteLine(Name);
        }
        public override void Sound()
        {
            Console.WriteLine("Звук інструменту: Уле-леее-ле-ле-ле");
        }
        public override string ToString()
        {
            return $"({Name}, {History}) - {Description}";
        }
    }
}
