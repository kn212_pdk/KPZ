﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    internal class Program
    {
        public class Facade
        {
            protected add Add;
            protected Reading reading;
            public Facade(add add, Reading read)
            {
                Add = add;
                reading = read;
            }
            public void Operation_add()
            {
                int x;
                Console.Write("Додайте назву справи: ");
                string u = Console.ReadLine();
                Console.Write("Встановіть пріорітет справи: ");

                while (int.TryParse(Console.ReadLine(), out x) == false)
                {
                    Console.WriteLine("Помилка введення, введіть значення ще раз: ");
                }

                Console.Write("Встановіть дату виконання: ");
                string d = Console.ReadLine();
                Add.Task(u);
                Add.Priority(x);
                Add.Date(d);
                Console.WriteLine();
            }
            public void Operation_read()
            {
                int f = 0;
                reading.V(ref f);

                for (int i = 1; i < f; i++)
                {
                    Console.WriteLine();
                    reading.Read_Prioritet(i);
                    Console.Write(" - ");
                    reading.Read_Sprava(i);
                    Console.Write("(");
                    reading.Read_Date(i);
                    Console.Write(")");
                    Console.WriteLine();
                    Console.WriteLine();
                }
            }
        }
        public class Reading
        {
            string[] arr;
            public void V(ref int f)
            {
                using (FileStream stream2 = File.OpenRead("Task.txt"))
                {
                    byte[] mass1 = new byte[stream2.Length];
                    stream2.Read(mass1, 0, mass1.Length);
                    string str = System.Text.Encoding.Default.GetString(mass1);
                    arr = str.Split('.');
                    f = arr.Length;
                }
            }
            public void Read_Sprava(int i)
            {
                string[] arr;
                using (FileStream stream2 = File.OpenRead("Task.txt"))
                {
                    byte[] mass1 = new byte[stream2.Length];
                    stream2.Read(mass1, 0, mass1.Length);
                    string str = System.Text.Encoding.Default.GetString(mass1);
                    arr = str.Split('.');
                    Console.Write(arr[i]);
                }
            }
            public void Read_Prioritet(int i)
            {
                string[] arr;
                using (FileStream stream2 = File.OpenRead("Priority.txt"))
                {
                    byte[] mass1 = new byte[stream2.Length];
                    stream2.Read(mass1, 0, mass1.Length);
                    string str = System.Text.Encoding.Default.GetString(mass1);
                    arr = str.Split('.');
                    Console.Write(arr[i]);
                }
            }
            public void Read_Date(int i)
            {
                string[] arr;
                using (FileStream stream2 = File.OpenRead("Date.txt"))
                {
                    byte[] mass1 = new byte[stream2.Length];
                    stream2.Read(mass1, 0, mass1.Length);
                    string str = System.Text.Encoding.Default.GetString(mass1);
                    arr = str.Split(' ');
                    Console.Write(arr[i]);
                }
            }
        }
        public class add
        {
            public void Task(string r)
            {
                using (FileStream stream1 = new FileStream("Task.txt", FileMode.Append))
                {
                    string y = Convert.ToString(r);
                    string x = ".";
                    byte[] b = System.Text.Encoding.Default.GetBytes(x);
                    byte[] c = System.Text.Encoding.Default.GetBytes(y);
                    stream1.Write(b, 0, b.Length);
                    stream1.Write(c, 0, c.Length);
                }
            }
            public void Priority(int r)
            {
                using (FileStream stream1 = new FileStream("Priority.txt", FileMode.Append))
                {
                    string y = Convert.ToString(r);
                    string x = ".";
                    byte[] b = System.Text.Encoding.Default.GetBytes(x);
                    byte[] c = System.Text.Encoding.Default.GetBytes(y);
                    stream1.Write(b, 0, b.Length);
                    stream1.Write(c, 0, c.Length);
                }
            }
            public void Date(string r)
            {
                using (FileStream stream1 = new FileStream("Date.txt", FileMode.Append))
                {
                    string y = Convert.ToString(r);
                    string x = " ";
                    byte[] b = System.Text.Encoding.Default.GetBytes(x);
                    byte[] c = System.Text.Encoding.Default.GetBytes(y);
                    stream1.Write(b, 0, b.Length);
                    stream1.Write(c, 0, c.Length);
                }
            }
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            add add1 = new add();
            Reading read1 = new Reading();
            Facade facade = new Facade(add1, read1);
            int a = -1;
            while (a != 0)
            {
                Console.WriteLine("1 - Переглянути свої справи");
                Console.WriteLine("2 - Додати справу");
                Console.WriteLine("0 - Вийти");
                Console.Write("Виберіть дію: ");

                while (int.TryParse(Console.ReadLine(), out a) == false)
                {
                    Console.WriteLine("Помилка введення, перевірте та введіть коректно: ");
                }

                switch (a)
                {
                    case 1:
                        {
                            facade.Operation_read();
                        }
                        break;
                    case 2:
                        {
                            facade.Operation_add();
                        }
                        break;
                }
            }
        }
    }
}

